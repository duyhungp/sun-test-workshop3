function categories (amount) {
    let category = '';
    if (amount < 1000)
        category = 'small';
    else if (amount < 5000)
        category = 'Mid small'
    else if (amount < 7000)
        category = 'Mid';
    else if (amount < 50000)
        category = 'Mid large';
    else if (amount < 100000)
        category = 'Large';
    return category;
}
